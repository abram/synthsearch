RandomSearch{
	var <>synth, <>compVec, <>params, <>spr;
	*new{arg synthName,analyzedVec, parameters=[\freq], ranges=[[50,1000]];
		^super.newCopyArgs(synthName, analyzedVec,parameters,ranges);
	}

	search{arg iters=1000, patience=100, def;
		var maxPatience, bestSet, waitTime, mfcc, error,bestErr, in=Array.newClear(this.params.size), pTemp=List.new, errorData=List.new;
		maxPatience = patience;
		//Routine.run{
		error = SynthSearchDif.new;
		this.spr.do{
			arg val, i;
			in[i] = rrand(val[0], val[1]);
		};
		mfcc = SynthSearchAnalysis.new(this.synth);
		in.do{
			arg val, i;
			pTemp.add(this.params[i]);
			pTemp.add(val);
		};
		pTemp.postln;
		mfcc.record(def, pTemp.asArray);
		pTemp.clear;
		//0.1.wait;
		//(waitTime+0.5).wait;
		//mfcc.analyze;
		error.calculate(this.compVec, mfcc.analysis);
		bestErr = error.err;
		{(iters > 0) && (patience > 0)}.while {
			("patience: " + patience).postln;
			this.spr.do{
				arg val, i;
				in[i] = rrand(val[0], val[1]);
			};
			in.do{
				arg val, i;
				pTemp.add(this.params[i]);
				pTemp.add(val);
			};
			pTemp.postln;
			mfcc.record(def, pTemp.asArray);
			//0.1.wait;
			//(waitTime+0.5).wait;
			//mfcc.analyze;
			error.calculate(this.compVec, mfcc.analysis);
			if(error.err < bestErr, { bestErr = error.err; patience = maxPatience; bestSet = pTemp.asArray; errorData.add(error.err);});
			pTemp.clear;
			iters = iters - 1;
			patience = patience - 1;
		};
		errorData.asArray.plot;
		^bestSet;
		//}
	}

	*search{arg synthName, analyzedVec, parameters, ranges, iterations, patience, def ;
		var inpt;
		^this.new(synthName, analyzedVec,parameters, ranges).search(iterations, patience, def);
	}
}