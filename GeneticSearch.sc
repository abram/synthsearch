//Genetic Search
GeneticSearch{
	var <>synth, <>compVec, <>params, <>spr, <>populace, <>errorData;
	*new{arg synthName, inputVector, params, ranges;
		^super.newCopyArgs(synthName,inputVector, params, ranges);
	}

	search{arg size, rounds, def,in=Array.fill(this.params.size, {arg i; rrand(this.spr[i][0], this.spr[i][1])});
		var mfcc, error, population=PriorityQueue.new, elites, mutants, kids, randoms, used=List.new, out, temp, waitTime;
		//initialize error;
		this.errorData = List.new;
		this.errorData.clear;
		//popError, 1 is
		//Routine.run{
		//initialize the population
		population.put(0,[0, in]);
		(size - 1).do{
			arg i;
			var t;
			population.put(0, [0,Array.fill(this.params.size, {arg i; rrand(this.spr[i][0], this.spr[i][1])})]);
		};
		mfcc = SynthSearchAnalysis.new(this.synth);
		error = SynthSearchDif.new;
		//eval for first time
		population.size.do{
			arg count;
			var temp, inParam=Array.new;
			("gen: 0, number:"+count).postln;
			temp = population.pop;
			temp[1].do{
				arg tVal, i;
				inParam = inParam++this.params[i]++tVal;
			};
			mfcc.record(def, inParam);
			//(waitTime+0.5).wait;
			//mfcc.analyze;
			error.calculate(this.compVec, mfcc.analysis);
			used.add([error.err, temp[1]]);
		};

		//refill the population queue to be automatically sorted
		used.size.do{
			arg i;
			population.put(used[i][0], used[i]);
		};
		this.errorData.add(population.topPriority);
		population.topPriority.postln;
		used = List.new;//resets the used


		for(0, (rounds - 1), {
			arg i;
			//creating new population pool
			elites = Array.newClear((population.size * 0.15).round.asInteger);//save top 20%
			mutants = Array.newClear((population.size *0.2).round.asInteger);//mutate 20-40%
			kids = Array.newClear((population.size*0.15).round.asInteger);//crossover 40-50%(crossover with the top 20%)
			randoms = Array.newClear((population.size *0.5).round.asInteger);//randomize bottom 50%
			elites.size.do{
				arg i;
				elites[i] = population.pop;
			};

			//create mutants
			mutants.size.do{
				arg i;
				//mutate
				mutants[i] = population.pop;
				mutants[i][1].size.do{
					arg pCount;
					if(rrand(0,1) < 0.5, {
						mutants[i][1][pCount] = (mutants[i][1][pCount] * 1.1).clip(this.spr[pCount][0], this.spr[pCount][1]);
					},{
						mutants[i][1][pCount] = (mutants[i][1][pCount] * 0.9).clip(this.spr[pCount][0], this.spr[pCount][1]);
					});
				};
				mutants[i][0] = 0;//reset it
			};

			population.clear;//kills off rest of population

			//crossover ones
			kids.size.do{
				arg i;
				var embryo=Array.newClear(this.params.size);
				embryo.size.do{
					arg gCounter;
					var genePoint=rrand(0, elites.size - 1);
					embryo[gCounter] = elites[genePoint][1][gCounter];
				};
				kids[i] = [0, embryo];//a lil child that has grown up
			};

			randoms.size.do{
				arg i;
				randoms[i] = [0, Array.fill(this.params.size, {arg i; rrand(this.spr[i][0], this.spr[i][1])})];
			};

			(elites++mutants++kids++randoms).do{
				arg val;
				population.put(val[0], val);
			};
			//evaluate
			population.size.do{
				arg count;
				var temp, inParam=Array.new;
				("gen:"+i+"number:"+count).postln;
				temp = population.pop;
				temp[1].do{
					arg tVal, i;
					inParam = inParam++this.params[i]++tVal;
				};
				mfcc.record(def, inParam);
				//(waitTime + 0.5).wait;
				//mfcc.analyze;
				error.calculate(this.compVec, mfcc.analysis);
				used.add([error.err, temp[1]]);
			};
			//population.size.postln;
			//refill the population queue to be automatically sorted
			used.size.do{
				arg i;
				population.put(used[i][0], used[i]);
			};
			population.topPriority.postln;
			this.errorData.add(population.topPriority);
			used = List.new;//resets the used


		});
		temp = population.pop;
		out = List.new;
		temp[1].do{
			arg val,i;
			out.add(this.params[i]);
			out.add(val);
		};
		this.errorData.asArray.plot;
		^out.asArray
		//};
	}

	*search{arg synthName, analyzedVector, parameters=[\freq], ranges=[[50,1000]], size, rounds, def, in;
		^this.new(synthName, analyzedVector, parameters, ranges).search(size, rounds, def, if(in.isNil, {Array.fill(parameters.size, {arg i; rrand(ranges[i][0], ranges[i][1])})}, {in}));
	}
}