# README #

### What is SynthSearch? ###

* SynthSearch is a SuperCollider library containing objects that can be used to accept a wav file of a synth, and then using a user written synth find the correct parameters to mimic that synth.
* Currently on v0.0.3

### Usage ###

##### RandomSearch.search(synthName, parameters, ranges, iters, patience, def) #####

* Parameters will only be the names of the parameters used in the synth (ie. [\freq, \atk, \sus, \rel])
* Ranges correspond to parameters based upon indices
* This and all search methods output a synth friendly parameter that can be directly put into a Synth.new to play.

##### TwiddleSearch.search(synthName, parameters, ranges, def, in) #####

* all are the exact same as above
* In can take in an input of possible parameters such as [400, 200, 300,400, 0.42345] 

##### GeneticSearch.search(synthName, parameters, ranges, pop, generations, def, in) #####

* Population defines the size of the population pool
* generations determines how many cycles it will go through

##### SynthSearchAnalysis.convertNum(comp, params) #####

* This will take a synth valid parameter input ([\freq, 440, \atk, 0.03, \sus, 0, \rel, 0.34]) and convert it into one usable for the search inputs([440, 0.03, 0, 0.34]) and is used to pipe searches into each other


### How do I get set up? ###

* Requires: Supercollider v 3.6+, SCMIR. 
* First install supercollider, and then install all SCMIR related libraries. 
* SCMIR may require rebuilding libgsl.

### Contribution guidelines ###

* Code review, check for bad code and create a PR for fixing unneeded stuff
* New features, fork and create a PR! 

### Who do I talk to? ###

* TODO: Update