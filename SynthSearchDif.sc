SynthSearchDif{
	var <>err;

	*new{ ^super.new }

	calculate{
		arg vec1, vec2;
		var cos, c1=0.0, c2=0.0, cn=0.0, euc=0, size;
		if(vec1.size >= vec2.size, {
			size = vec2.size;
		}, {
			size = vec1.size;
		});
		//Cosine similarity
		for(0, size - 1, {
			arg i;
			cn = cn + (vec1[i] * vec2[i]);
			c1 = c1 + vec1[i].squared;
			c2 = c2 + vec2[i].squared;
		});
		cos = (1.0 - (cn/((c1.sqrt) * (c2.sqrt))));
		//cos.postln;
		//euclidean distance
		for(0, size - 1, {
			arg i;
			euc = euc + (vec1[i] - vec2[i]).squared;
		});
		euc = euc.sqrt;
		//euc.postln;
		//combining the two
		//this.err = euc;
		this.err = (0.4 * cos) + (0.6 * ((1 / (((1 + pow(0.5772156649, (-1 * euc)))))) - 0.5).abs)
		//this.err =(0.2 * ((1/(1+pow(0.5772156649, (-1 * (vec2 - vec1).abs)))) - 0.5).abs) + (0.4 * cos) + (0.4 * ((1 / (((1 + pow(0.5772156649, (-1 * euc)))))) - 0.5).abs);
	}
}


