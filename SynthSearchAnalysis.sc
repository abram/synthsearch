SynthSearchAnalysis {
	var <>synth,  <>featureinfo, <>numfeatures,<>normalizationtype, <>numframes, <>featuredata, <>analysis, <>mfcc;
	*new {
		arg synthName;
		^super.newCopyArgs(synthName);

	}

	record{ arg def, argumentArray=[\freq, 440];
		var time;
		//WINDOWS CHANGE TO \SynthSearch\tmp\temp.wav
		//time = SynthSearchAnalysis.times(argumentArray);
		//Server.local.record(Platform.userExtensionDir++"/SynthSearch/tmp/temp.wav");
		//Synth.new(synth, argumentArray);
		//this.featuredata = [[MFCC, 13]];
		//this.numfeatures = 13;
		//argumentArray.postln;
		this.analysis = this.extractFeatures(def, argumentArray);

		//this.analysis.postln;
		//SystemClock.sched(time, { Server.local.stopRecording; });
		//^time;
	}
	//WINDOWS CHANGE TO \SynthSearch\tmp\temp.wav
	*analyze{arg file=Platform.userExtensionDir++"/SynthSearch/tmp/temp.wav";
		var mfcc, d;
		mfcc = SCMIRAudioFile(file, [[MFCC, 13]]);
		mfcc.extractFeatures();
		//this.mfcc.featuredata;
		^mfcc.featuredata;//this.averageWav;
		//this.analysis;
	}
	averageWav{
		var avg=Array.fill(this.mfcc.numfeatures, {0});
		this.mfcc.featuredata.do{
			arg val, i;
			avg[i % this.mfcc.numfeatures] = avg[i % this.mfcc.numfeatures] + val;
		};
		avg.do {
			arg val, i;
			avg[i] = val / this.mfcc.numframes;
		};
		^avg;
	}

	averageRec{
		var avg=Array.fill(this.numfeatures,{0});
		this.featuredata.do{
			arg val, i;
			avg[i % this.numfeatures] = avg[i % this.numfeatures] + val;
		};
		avg.do{
			arg val, i;
			avg[i] = val / this.numframes;
		};
		^avg;
	}

	*times{
		arg params;
		var out=0;
		//params.postln;
		params.do{
			arg val, i;
			if((val == \atk) || (val == \sus) ||( val == \rel), {
				out = out + params[i+1];
			});
		};
		^out;
	}

	*convertNum{
		//helper method to feed in the params from the other two
		arg comp, params;
		var dict=Dictionary.new, out = List.new;
		params.do{
			arg val, i;
			if(i % 2 == 1, {
				dict.add(params[i-1] -> val);
				//out.add(val);
			});
		};
		comp.do{
			arg val;
			out.add(dict.at(val));
		};
		^out.asArray;
	}


	extractFeatures{
		arg def, featureparams, normalize=true, useglobalnormalization=false;
		var file, onsetdata, temp, analysisfilename, serveroptions, score, duration, nodeID,ugenindex, buffersize;
		//this.featuredata = [[MFCC, 13]];
		this.featureinfo = [[MFCC,13]];
		this.numfeatures = 13;
		this.normalizationtype = 0;
		def.children.do{|val, i| if(val.class==FeatureSave, {ugenindex = val.synthIndex})};
		//ugenindex.postln;
		def.writeDefFile;
		analysisfilename = SCMIR.tempdir++this.synth++"features.data";
		duration = SynthSearchAnalysis.times(featureparams);
		//duration.postln;
		buffersize = ((44100*duration)/SCMIR.framehop).asInteger;
		score = [
			[0.0, [\s_new, this.synth, 1000, 0, 0]++featureparams],

			[0.01, [\u_cmd, 1000, ugenindex, "createfile", analysisfilename]],
			[duration, [\u_cmd, 1000, ugenindex, "closefile"]],
			[duration,[\c_set, 0, 0]]
		];
		serveroptions = ServerOptions.new;
		serveroptions.numOutputBusChannels = 1; // mono output
		Score.recordNRTSCMIR(score,SCMIR.nrtanalysisfilename,SCMIR.nrtoutputfilename, nil,44100, "WAV", "int16", serveroptions); // synthesize
		file = SCMIRFile(analysisfilename,"rb");

		this.numframes = file.getInt32LE;
		//numframes.postln;
		temp = file.getInt32LE;
		//temp.postln;
		if (numfeatures!= temp) {
			"extract features: mismatch of expectations in number of features ".postln;
			[numfeatures, temp].postln;
		};

		temp = this.numframes*this.numfeatures;
		//temp.postln;
		featuredata = FloatArray.newClear(temp);

		file.readLE(this.featuredata);
		//featuredata.postln;
		if((featuredata.size) != temp) {

			file.close;

			featuredata = nil;

			SCMIR.clearResources;

			file = SCMIRFile(analysisfilename,"rb");
			file.getInt32LE;
			file.getInt32LE.postln;

			onsetdata = FloatArray.newClear(temp);
			file.readLE(this.featuredata);
		};


		file.close;
		if(normalize && (numframes>=1)){this.featuredata = this.normalize(this.featuredata,false,useglobalnormalization);   };
		("rm "++ (analysisfilename.asUnixPath)).systemCmd;

		"Feature extraction complete".postln;
		^featuredata
	}


	normalize { |array, getstats=false, useglobal=false|

		var offset;
		var temp1, temp2, stats;
		this.normalizationtype = 0;


		offset=0;

		//numframes = 	SCMIR.soundfile.numFrames-1;

		if(getstats) {

			//if( (normalizationtype == 0) or: (normalizationtype == 1)) {

			//not needed for normtype 2 but preserved for sake of other code
				stats = [nil!numfeatures,nil!numfeatures];
			//}
			//{
				//stats = nil!numfeatures; //providing whole set of numbers for quantile normalisation
			//};

		};

		featureinfo.do{|featurenow|

			var numberlinked = 1;
			var numfeaturesnow = 1;
			var normalizationtype = 0;

			//featurenow.postln;

			//normtype now preset; just Chromagram as multi feature norm

			switch(featurenow[0].asSymbol,
				\MFCC,{
					numfeaturesnow = featurenow[1];

					//this means do overall normalization
					if(featurenow.size>=3,{

						numfeaturesnow = 1;
						numberlinked = featurenow[1];
					});
				},
				\Chromagram,{

					numberlinked = featurenow[1];
				},
				\SpectralEntropy,{

					//numberlinked = featurenow[1];
					//don't inter-link by default
					numfeaturesnow = featurenow[1];
				},
				\Tartini,{
					numfeaturesnow = if(featurenow.size==1,1,2);
				},
				\OnsetStatistics,{
					numfeaturesnow = 3;
				},
				\BeatStatistics,{
					numfeaturesnow = 4;
				},
				\CustomFeature,{
					//1 output only if nil, else supplied
					numfeaturesnow = (featurenow[2]?1);

					//this means do overall normalization
					if(featurenow.size>=4,{

						numberlinked = numfeaturesnow;
						numfeaturesnow = 1;

					});

				},
				\PolyPitch,{

					numfeaturesnow = (2*featurenow[1])+1;
				},
				\PianoPitch,{

					numfeaturesnow = 88;
				}
			);


			if(getstats) {

				numfeaturesnow.do{|i|

					switch(normalizationtype,0,{

						//normalize

						#temp1, temp2 = this.featureRange(array,offset,numberlinked);

						stats[0][offset..(offset+numberlinked-1)] = temp1;
						stats[1][offset..(offset+numberlinked-1)] = temp2;

						//[temp1, temp2];
					},1,{

						//standardize

						temp1 = this.featureMean(array,offset,numberlinked);
						temp2 = this.featureVariance(array,offset,numberlinked,temp1).sqrt;

						//stats[offset..(offset+numberlinked-1)] = [temp1, temp2];
						stats[0][offset..(offset+numberlinked-1)] = temp1;
						stats[1][offset..(offset+numberlinked-1)] = temp2;

					},{
						//default as last function in switch


							//[featurenow,numberlinked, numfeaturesnow, stats.size].postln;

						//quantile; take negative of normalizationtype for number of quantiles, default 10

						//var numquantiles = 10; //10% quantiles
						//var target;
						//if(normalizationtype.isInteger && normalizationtype<0) {numquantiles = normalizationtype.neg; };


						temp1 = Array.fill(numberlinked,{|j| array[offset+j,numfeatures+offset+j..]}).flatten;

						//temp1 = this.featureQuantiles(array,offset,numberlinked,numquantiles);
						//..(offset+numberlinked-1)
						stats[0][offset] = temp1; //[temp1,numquantiles]; //nils in other entries for linked since no use, will need to write back later once have results

						//if(numberlinked>1) {
						//stats[offset+1..(offset+numberlinked-1)] = nil;
						//};

						//target = Array.fill(numberlinked,{|i| array[offset+i,numfeatures+offset+i..]}).flatten;

						//requires MathLib extension quark

						//(numquantiles-1)
						//stats[offset..(offset+numberlinked-1)] = target.percentile((1.0/numquantiles)*(0,1..numquantiles));
						//after normalisation top always 1.0?

					});






					offset = offset + numberlinked;
				};


			} {

				numfeaturesnow.do{|i|

					switch(normalizationtype,0,{

						//normalize

						array = this.normalizeFeature(array,offset,numberlinked,useglobal);

					},1,{

						//standardize

						array = this.standardizeFeature(array,offset,numberlinked,useglobal);

					},{

						//quantile
						var numquantiles = 10; //10% quantiles

						if(normalizationtype.isInteger && normalizationtype<0) {numquantiles = normalizationtype.neg; };

						array = this.quantileFeature(array,offset,numberlinked,numquantiles,useglobal);

					});

					offset = offset + numberlinked;
				}

			}

		}

		^if(getstats){stats}{array};
	}
		normalizeFeature { |array, which=0, num=1, useglobal=false|

		var maxval, minval, maxminusminr;
		var top= num-1;
		var temp, val;


		if(useglobal){

			#minval, maxval = SCMIR.lookupGlobalFeatureNorms(which);
		} {

			#minval, maxval = this.featureRange(array,which,num);
		};


		if(maxval!=minval) {

			maxminusminr = 1.0/(maxval-minval);

			temp = which;

			for(0,this.numframes-1,{|i|

				for(temp,temp+top,{|j|

					val = array[j];

					array[j] = (val-minval)*maxminusminr;

				});

				temp = temp+ numfeatures;
			});

		} {

			//everything is already just one value, leave array alone

		};

		^array;
	}
		featureRange {|array,which=0,num=1|

		var val, minval, maxval;
		var temp;
		var top= num-1;

		temp = which;

		minval = array[temp];
		maxval = array[temp];

		for(0,this.numframes-1,{|i|

			for(temp,temp+top,{|j|

				val = array[j];

				if (val>maxval,{maxval= val;});
				if (val<minval,{minval= val;});


			});

			temp = temp + numfeatures;
		});


		^[minval, maxval];

	}



}



